package com.geeklabs.pms.transformer;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dozer.DozerBeanMapper;

import com.geeklabs.pms.domain.Billing;
import com.geeklabs.pms.domain.Medicine;
import com.geeklabs.pms.domain.Rack;
import com.geeklabs.pms.dto.BillingDto;
import com.geeklabs.pms.dto.MedicineDto;
import com.geeklabs.pms.dto.RackDto;

public class Transformer {

	public static List<MedicineDto> convertMedicineListtoMedicineDtoList(List<Medicine> medicines, DozerBeanMapper dozerBeanMapper) {
		List<MedicineDto> medicineDtos = new ArrayList<MedicineDto>();
		for (Medicine medicine : medicines) {
			MedicineDto medicineDto = dozerBeanMapper.map(medicine, MedicineDto.class);
			Date expiryDate = medicine.getExpiryDate();
			Format formatter = new SimpleDateFormat("yyyy-MM-dd");
			String expireDate = formatter.format(expiryDate);
			medicineDto.setExpireDate(expireDate);
			medicineDtos.add(medicineDto);
		}
		return medicineDtos;

	}

	public static List<RackDto> convertRackListtoRackDtoList(List<Rack> racks, DozerBeanMapper dozerBeanMapper) {
		List<RackDto> rackDtos = new ArrayList<RackDto>();
		for (Rack rack : racks) {
			RackDto rackDto = dozerBeanMapper.map(rack, RackDto.class);
			rackDtos.add(rackDto);
		}
		return rackDtos;

	}

	public static List<BillingDto> convertBillingListToBillingDtoList(List<Billing> billings, DozerBeanMapper dozerBeanMapper) {
		List<BillingDto> billingDtos = new ArrayList<BillingDto>();
		for (Billing billing : billings) {
			BillingDto billingDto = dozerBeanMapper.map(billing, BillingDto.class);
			Date billingDate = billing.getDate();
			Format formatter = new SimpleDateFormat("yyyy-MM-dd");
			String date = formatter.format(billingDate);
			billingDto.setCreatedDate(date);
			billingDtos.add(billingDto);
		}
		return billingDtos;
	}

}
