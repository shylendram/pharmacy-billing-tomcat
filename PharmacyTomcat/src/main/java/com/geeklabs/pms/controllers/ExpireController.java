package com.geeklabs.pms.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.pms.dto.MedicineDto;
import com.geeklabs.pms.service.ExpireService;
import com.geeklabs.pms.util.RequestMapper;

@Controller
public class ExpireController {
	
	 @Autowired
     private ExpireService expireService;
	 
	@RequestMapping(value = RequestMapper.EXPIRE, method = RequestMethod.GET)
	public String medicine(Model map) {
		return "expire";
	}
	
	@ResponseBody
	@RequestMapping(value = RequestMapper.EXPIRE_LIST, method = RequestMethod.GET, produces = "application/json")
	public List<MedicineDto> getExpiryMedicine() {
		return expireService.getExpiryMedicine();
	}

}
