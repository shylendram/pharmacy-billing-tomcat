package com.geeklabs.pms.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.geeklabs.pms.config.Message;
import com.geeklabs.pms.dto.MedicineDto;
import com.geeklabs.pms.dto.RackDto;
import com.geeklabs.pms.service.MedicineService;
import com.geeklabs.pms.service.RackService;
import com.geeklabs.pms.util.RequestMapper;
import com.geeklabs.pms.util.ResponseStatus;

@Controller
@RequestMapping(value = "medicine")
public class MedicineController {

	@Autowired
	private MedicineService medicineService;
	
	@Autowired
	private RackService rackService;

	@RequestMapping(method = RequestMethod.GET)
	public String medicine(Model map) {
		return "medicine";
	}

	@ResponseBody
	@RequestMapping(value = RequestMapper.MEDICINE_LIST, method = RequestMethod.GET, produces = "application/json")
	public List<MedicineDto> getMedicineList() {

		List<MedicineDto> allMedicines = medicineService.getAllMedicine();
		return allMedicines;

	}

	@RequestMapping(value = RequestMapper.ADDMEDICINE, method = RequestMethod.GET)
	public String addMedicine(Model map) {
		MedicineDto medicineDto = new MedicineDto();
		map.addAttribute("medicine", medicineDto);
		
		//Send List of Rack names to front end
		List<RackDto> racks = rackService.getAllRacks();
		map.addAttribute("racks", racks);

		return "addMedicine";

	}

	@RequestMapping(value = RequestMapper.UPDATE_MEDICINE, method = RequestMethod.GET)
	public String addMedicine(Model map, @PathVariable Long medicineId) {
		MedicineDto medicineDto = medicineService.getMedicinebyId(medicineId);
		map.addAttribute("medicine", medicineDto);
		map.addAttribute("racks", medicineDto.getRacks());
		return "addMedicine";
	}

	@RequestMapping(value = RequestMapper.ADDMEDICINE, method = RequestMethod.POST)
	public String createMedcine(@ModelAttribute("medicine") MedicineDto medicineDto, BindingResult bindingResult,
			final RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest, Model map) {
		// save
		if (medicineDto.getId() == null) {
			ResponseStatus responseStatus = medicineService.saveMedicine(medicineDto);
			if (responseStatus.getStatus() != null && "success".equalsIgnoreCase(responseStatus.getStatus())) {
				redirectAttributes.addFlashAttribute("msg", new Message("Medicine saved successfully.", Message.SUCCESS));
			} else {
				redirectAttributes.addFlashAttribute("msg", new Message("Error occured while saving the Medicine.", Message.ERROR));
			}
		}
		// update
		else {
			ResponseStatus responseStatus = medicineService.updateMedicine(medicineDto);
			if (responseStatus.getStatus() != null && "success".equalsIgnoreCase(responseStatus.getStatus())) {
				redirectAttributes.addFlashAttribute("msg", new Message("Medicine Updated successfully.", Message.SUCCESS));
			} else {
				redirectAttributes.addFlashAttribute("msg", new Message("Error occured while updating the Medicine.", Message.ERROR));
			}
		}
		return "medicine";
	}
	
	@ResponseBody
	@RequestMapping(value = RequestMapper.DELETE_MEDICINE, method = RequestMethod.GET, produces = "application/json")
	public ResponseStatus deleteMedicine(@PathVariable Long medicineId){
		ResponseStatus responseStatus = medicineService.deleteMedicine(medicineId);
		return responseStatus;
	}
	
	@RequestMapping(value = RequestMapper.GET_MEDICINES_BY_FILTER, method = RequestMethod.GET)
	public @ResponseBody List<MedicineDto> getMedicinesListByName(@PathVariable String medicineName){
		List<MedicineDto> allMedicines = medicineService.getAllMedicinesByMedicineName(medicineName);
		return allMedicines;
	}
}
