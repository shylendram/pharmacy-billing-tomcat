package com.geeklabs.pms.controllers;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.pms.dto.BillingDto;
import com.geeklabs.pms.service.BillingService;
import com.geeklabs.pms.util.RequestMapper;
import com.geeklabs.pms.util.ResponseStatus;

@Controller
@RequestMapping(value = RequestMapper.BILLING)
public class BillingController {

	@Autowired
	private BillingService billingService;

	@RequestMapping(method = RequestMethod.GET)
	public String billing(Model map) {
		return "billing";
	}

	@RequestMapping(value = RequestMapper.ADD_BILLING, method = RequestMethod.GET)
	public String addBilling(Model map) {
		BillingDto billingDto = new BillingDto();
		map.addAttribute("billing", billingDto);
		return "addBilling";
	}
	
	@ResponseBody
	@RequestMapping(value = RequestMapper.GET_BILLING_LIST, method = RequestMethod.GET, produces = "application/json")
	public List<BillingDto> getBillingList() {
		return billingService.getAllBillings();
	}
	
	@ResponseBody
	@RequestMapping(value = RequestMapper.ADD_BILLING, method = RequestMethod.POST, produces = "application/json")
	public ResponseStatus createBilling(@RequestBody BillingDto billingDto, final Principal principal) {
		// save
		ResponseStatus responseStatus = new ResponseStatus();
		if (billingDto.getId() == null) {
			responseStatus = billingService.saveBilling(billingDto);
		}
		// update
		/*
		 * else { ResponseStatus responseStatus =
		 * billingService.updateBilling(billingDto); if
		 * (responseStatus.getStatus() != null &&
		 * "success".equalsIgnoreCase(responseStatus.getStatus())) {
		 * redirectAttributes.addFlashAttribute("msg", new
		 * Message("Billing Updated successfully.", Message.SUCCESS)); } else {
		 * redirectAttributes.addFlashAttribute("msg", new
		 * Message("Error occured while updating the Billing.", Message.ERROR));
		 * } }
		 */
		return responseStatus;
	}
	
	@ResponseBody
	@RequestMapping(value = RequestMapper.GET_PATIENT_ID,method = RequestMethod.GET,produces = "application/json")
	public String getPatientId() {
		return billingService.getPatientId();
	}
	
	@ResponseBody
	@RequestMapping(value = RequestMapper.GET_BILL_NO, method = RequestMethod.GET, produces = "application/json")
	public Long getBillNo(){
		return billingService.getBillNo();
	}
	@ResponseBody
	@RequestMapping(value = RequestMapper.GET_BILL_BY_PATIENT_ID,method = RequestMethod.GET,produces = "application/json")
	public List<BillingDto> getBilligInfo(@PathVariable String patientId) {
		return billingService.getBilligInfoByPatientId(patientId);
	}

}
