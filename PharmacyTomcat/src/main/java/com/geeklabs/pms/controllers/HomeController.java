package com.geeklabs.pms.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.geeklabs.pms.util.RequestMapper;

@Controller 
public class HomeController {

//	@Autowired
//	private HomeService homeService;
	
	@RequestMapping(value = "/" , method = RequestMethod.GET)
	public String login(Model map) {
		return "login";
	}

	@RequestMapping(value = RequestMapper.HOME, method = RequestMethod.GET)
	public String home(Model map) {
		return "home";
	}
}
