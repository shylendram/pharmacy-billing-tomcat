package com.geeklabs.pms.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.geeklabs.pms.config.Message;
import com.geeklabs.pms.dto.RackDto;
import com.geeklabs.pms.service.RackService;
import com.geeklabs.pms.util.RequestMapper;
import com.geeklabs.pms.util.ResponseStatus;

@Controller
public class RackController {
	
	@Autowired
	private RackService rackService;
	
	@RequestMapping(value = RequestMapper.RACK, method = RequestMethod.GET)
	public String rack(Model map) {
		return "rack";
	}
	
	@ResponseBody
	@RequestMapping(value = RequestMapper.RACK_LIST, method = RequestMethod.GET, produces = "application/json")
	public List<RackDto> getRackList(){
		
		List<RackDto> allRacks = rackService.getAllRacks();
		return allRacks;
		
	}
	
	@RequestMapping(value = RequestMapper.ADD_RACK, method = RequestMethod.GET)
	public String addRack(Model map){
		RackDto rackDto = new RackDto();
		map.addAttribute("rack",rackDto );
		
		return "addRack";
	
	}
	
	@RequestMapping(value = RequestMapper.UPDATE_RACK, method = RequestMethod.GET)
	public String addRack(Model map,@PathVariable Long rackId){
		RackDto rackDto = rackService.getRackbyId(rackId);
		map.addAttribute("rack", rackDto);
		return "addRack";
		
	}
	
	@RequestMapping(value = RequestMapper.ADD_RACK, method = RequestMethod.POST)
	public String createRack(@ModelAttribute("rack") RackDto rackDto,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest, Model map){
		//save
		if(rackDto.getId() == null){
			ResponseStatus responseStatus = rackService.saveRack(rackDto);
			if (responseStatus.getStatus() != null && "success".equalsIgnoreCase(responseStatus.getStatus())) {
				redirectAttributes.addFlashAttribute("msg", new Message("Rack saved successfully.", Message.SUCCESS));
			} else {
				redirectAttributes.addFlashAttribute("msg", new Message("Error occured while saving the Rack.", Message.ERROR));
			}
		//update	
		}else{
			ResponseStatus responseStatus = rackService.updateRack(rackDto);
			if (responseStatus.getStatus() != null && "success".equalsIgnoreCase(responseStatus.getStatus())) {
				redirectAttributes.addFlashAttribute("msg", new Message("Rack Updated successfully.", Message.SUCCESS));
			} else {
				redirectAttributes.addFlashAttribute("msg", new Message("Error occured while updating the Rack.", Message.ERROR));
			}
		}
		return "rack";
	}
	
	@ResponseBody
	@RequestMapping(value = RequestMapper.DELETE_RACK, method = RequestMethod.GET,produces = "application/json")
	public ResponseStatus deleteRack(@PathVariable Long rackId){
		ResponseStatus responseStatus = rackService.deleteRack(rackId);
		return responseStatus;
	}

}
