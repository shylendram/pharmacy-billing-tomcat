package com.geeklabs.pms.dto;

public class RackDto {

	private Long id;
	private String rackName;
	private Long rackColoumnNo;
	private Long rackRowNo;
	private String description;
	public String getRackName() {
		return rackName;
	}
	public void setRackName(String rackName) {
		this.rackName = rackName;
	}
	public Long getRackColoumnNo() {
		return rackColoumnNo;
	}
	public void setRackColoumnNo(Long rackColoumnNo) {
		this.rackColoumnNo = rackColoumnNo;
	}
	public Long getRackRowNo() {
		return rackRowNo;
	}
	public void setRackRowNo(Long rackRowNo) {
		this.rackRowNo = rackRowNo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	
}
