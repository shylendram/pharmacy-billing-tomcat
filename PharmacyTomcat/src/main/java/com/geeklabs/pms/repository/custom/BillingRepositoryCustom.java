package com.geeklabs.pms.repository.custom;

import java.util.List;

import com.geeklabs.pms.domain.Billing;

public interface BillingRepositoryCustom {

	long getMaxCodeNumber();
	List<Billing> getBillingInfoByPatientId(String patientId);
	List<Billing> findAllByCreatedDate(Billing billing);
}
