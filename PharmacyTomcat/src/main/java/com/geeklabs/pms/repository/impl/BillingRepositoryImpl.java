package com.geeklabs.pms.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.geeklabs.pms.domain.Billing;
import com.geeklabs.pms.repository.custom.BillingRepositoryCustom;

@Repository
public class BillingRepositoryImpl implements BillingRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	protected EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public long getMaxCodeNumber() {
		Object singleResult = entityManager.createQuery("select max(s.code) from " + Billing.class.getName() + " s").getSingleResult();
		if (singleResult == null) {
			return 0;
		}
		return (long) singleResult;
	}

	@Override
	public List<Billing> getBillingInfoByPatientId(String patientId) {
		CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Billing> createQuery = criteriaBuilder.createQuery(Billing.class);

		Root<Billing> root = createQuery.from(Billing.class);
		createQuery.select(root);

		Predicate equalPatientId = criteriaBuilder.equal(root.get("patientId"), patientId);

		createQuery.where(equalPatientId);

		try {
			List<Billing> resultList = getEntityManager().createQuery(createQuery).getResultList();
			return resultList;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return new ArrayList<>();
	}

	@Override
	public List<Billing> findAllByCreatedDate(Billing billing) {
		CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Billing> createQuery = criteriaBuilder.createQuery(Billing.class);

		Root<Billing> root = createQuery.from(Billing.class);
		createQuery.select(root);
		createQuery.orderBy(criteriaBuilder.desc(root.get("createdDate")));

		try {
			List<Billing> resultList = getEntityManager().createQuery(createQuery).getResultList();
			return resultList;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return new ArrayList<>();
	}

}
