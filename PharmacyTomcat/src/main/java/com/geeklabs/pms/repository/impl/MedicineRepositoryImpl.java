package com.geeklabs.pms.repository.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.geeklabs.pms.domain.Medicine;
import com.geeklabs.pms.repository.custom.MedicineRepositoryCustom;

@Repository
public class MedicineRepositoryImpl implements MedicineRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;
	
	public EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public List<Medicine> getExpiryMedicine() {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Medicine> createQuery = criteriaBuilder.createQuery(Medicine.class);

		Root<Medicine> root = createQuery.from(Medicine.class);
		createQuery.select(root);

		Predicate expMedicine = criteriaBuilder.lessThanOrEqualTo(root.<Date> get("expiryDate"), new Date());
		createQuery.where(expMedicine);
		try {
			List<Medicine> medicines = entityManager.createQuery(createQuery).getResultList();
			return medicines;
		} catch (Exception e) {

		}
		return new ArrayList<>();
	}

	@Override
	public List<Medicine> getAllMedicinesByMedicineName(String name) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Medicine> createQuery = criteriaBuilder.createQuery(Medicine.class);

		Root<Medicine> root = createQuery.from(Medicine.class);
		createQuery.select(root);
		Expression<String> path = root.get("medicineName");

		Predicate lesserThanOrEqualTo = criteriaBuilder.lessThanOrEqualTo(path, name + "\uFFFD");
		Predicate greaterThanOrEqualTo = criteriaBuilder.greaterThanOrEqualTo(path, name);

		createQuery.where(greaterThanOrEqualTo, lesserThanOrEqualTo);
		try {
			List<Medicine> medicines = entityManager.createQuery(createQuery).getResultList();
			return medicines;
		} catch (Exception e) {
		}
		return new ArrayList<>();
	}

	@Override
	public List<Medicine> findAllByCreatedDate(Medicine medicine) {
		CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Medicine> createQuery = criteriaBuilder.createQuery(Medicine.class);
		
		Root<Medicine> root = createQuery.from(Medicine.class);
		createQuery.select(root);
		createQuery.orderBy(criteriaBuilder.desc(root.get("createdDate")));
		
		try {
			List<Medicine> resultList = getEntityManager().createQuery(createQuery).getResultList();
			return resultList;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return new ArrayList<>();
	}
	
	@Override
	public void merge(Medicine medicine) {
		getEntityManager().merge(medicine);
		
	}
}
