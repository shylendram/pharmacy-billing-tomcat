package com.geeklabs.pms.repository.custom;

import java.util.List;

import com.geeklabs.pms.domain.User;

public interface UserRepositoryCustom {

	User getUserByEmailOrUserName(String userName, String email);
	User getUserByName(String userName);
	List<User> getUsersByUserRole(String name);
}
