package com.geeklabs.pms.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.geeklabs.pms.domain.User;
import com.geeklabs.pms.repository.custom.UserRepositoryCustom;

@Repository
public class UserRepositoryImpl implements UserRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public User getUserByName(String userName) {
		try {
			User user = (User) entityManager.createQuery("select user from " + User.class.getName() + " user where user.userName='"+userName+"'").getSingleResult();
			return user;
		} catch (Exception e) {
			// Don't handle, if any exception just return null, so that we can assume that no object found with gievn query
		}
		return null;
	}
	
	@Override
	public User getUserByEmailOrUserName(String userName, String email) {
		
		try {
			if (email.contains("@")) {
				User user = (User) entityManager.createQuery("select user from " + User.class.getName() + " user where user.email='" +email+ "'").getSingleResult();
				return user;				
			} else {
				User user = (User) entityManager.createQuery("select user from " + User.class.getName() + " user where user.userName='"+userName+"'").getSingleResult();
				return user;
			}
		} catch (Exception e) {
			// Don't handle, if any exception just return null, so that we can assume that no object found with gievn query
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public List<User> getUsersByUserRole(String userRole) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<User> createQuery = criteriaBuilder.createQuery(User.class);

		Root<User> root = createQuery.from(User.class);
		createQuery.select(root);

		Predicate equalUserRole = criteriaBuilder.equal(root.get("userRole"), userRole);
		createQuery.where(equalUserRole);
		
//		createQuery.orderBy(criteriaBuilder.desc(root.get("createdDate")));

		try {
			List<User> users = entityManager.createQuery(createQuery).getResultList();
			return users;
		} catch (Exception e) {
		}
		return new ArrayList<>();
	}
}
