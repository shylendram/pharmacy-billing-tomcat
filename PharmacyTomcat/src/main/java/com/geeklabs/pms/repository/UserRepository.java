package com.geeklabs.pms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.geeklabs.pms.domain.User;
import com.geeklabs.pms.repository.custom.UserRepositoryCustom;

public interface UserRepository extends UserRepositoryCustom, JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

}
