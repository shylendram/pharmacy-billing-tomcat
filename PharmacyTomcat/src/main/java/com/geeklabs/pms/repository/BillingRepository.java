package com.geeklabs.pms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.geeklabs.pms.domain.Billing;
import com.geeklabs.pms.repository.custom.BillingRepositoryCustom;

public interface BillingRepository extends BillingRepositoryCustom, JpaRepository<Billing, Long>, JpaSpecificationExecutor<Billing> {

}
