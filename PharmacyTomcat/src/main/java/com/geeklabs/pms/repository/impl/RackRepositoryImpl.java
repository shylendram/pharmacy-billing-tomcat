package com.geeklabs.pms.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.geeklabs.pms.domain.Rack;
import com.geeklabs.pms.repository.custom.RackRepositoryCustom;

@Repository
public class RackRepositoryImpl implements RackRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	protected EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public List<Rack> findAllByCreatedDate(Rack rack) {
		CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Rack> createQuery = criteriaBuilder.createQuery(Rack.class);

		Root<Rack> root = createQuery.from(Rack.class);
		createQuery.select(root);
		createQuery.orderBy(criteriaBuilder.desc(root.get("createdDate")));

		try {
			List<Rack> resultList = getEntityManager().createQuery(createQuery).getResultList();
			return resultList;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return new ArrayList<>();
	}

	@Override
	public void merge(Rack rack) {
		getEntityManager().merge(rack);

	}

}
