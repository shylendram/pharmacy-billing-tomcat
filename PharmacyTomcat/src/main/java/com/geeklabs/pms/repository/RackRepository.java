package com.geeklabs.pms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.geeklabs.pms.domain.Rack;
import com.geeklabs.pms.repository.custom.RackRepositoryCustom;

public interface RackRepository extends RackRepositoryCustom, JpaRepository<Rack, Long>, JpaSpecificationExecutor<Rack> {

}
