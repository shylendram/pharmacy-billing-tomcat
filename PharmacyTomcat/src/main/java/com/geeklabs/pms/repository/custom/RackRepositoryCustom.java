package com.geeklabs.pms.repository.custom;

import java.util.List;

import com.geeklabs.pms.domain.Rack;

public interface RackRepositoryCustom {

	List<Rack> findAllByCreatedDate(Rack rack);
	void merge(Rack rack);
}
