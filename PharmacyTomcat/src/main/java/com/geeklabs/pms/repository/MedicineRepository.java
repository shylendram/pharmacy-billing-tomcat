package com.geeklabs.pms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.geeklabs.pms.domain.Medicine;
import com.geeklabs.pms.repository.custom.MedicineRepositoryCustom;

public interface MedicineRepository extends MedicineRepositoryCustom, JpaRepository<Medicine, Long>, JpaSpecificationExecutor<Medicine> {

}
