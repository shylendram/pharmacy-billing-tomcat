package com.geeklabs.pms.repository;

import com.geeklabs.pms.domain.UserRole;
import com.geeklabs.pms.domain.enums.UserRoles;
import com.geeklabs.pms.util.CRUD;

public interface UserRoleRepository extends CRUD<UserRole> {
	
	UserRole getUserRoleByRoleName(UserRoles roleName);
}
