package com.geeklabs.pms.repository.custom;

import java.util.List;

import com.geeklabs.pms.domain.Medicine;

public interface MedicineRepositoryCustom {

	List<Medicine> getExpiryMedicine();
	List<Medicine> getAllMedicinesByMedicineName(String name);
	List<Medicine> findAllByCreatedDate(Medicine medicine);
	void merge(Medicine medicine);
}
