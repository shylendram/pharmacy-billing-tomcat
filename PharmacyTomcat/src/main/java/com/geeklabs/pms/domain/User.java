package com.geeklabs.pms.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

import com.geeklabs.pms.domain.enums.UserRoles;
import com.geeklabs.pms.domain.enums.UserStatus;
import com.google.common.base.Objects;

@Entity
public class User {

	@Id
	@GeneratedValue
	private Long id;
	
	//@NotNull
	//@Column(unique = true)
	private String email;
	
	private String firstName;
	private String lastName;

	@Enumerated(EnumType.STRING)
	private UserRoles userRole;

	//@NotNull
	//@Size(min = 6, max = 50)
	private String userName;
	
	//@NotNull
	private String password;
	
	//@NotNull
	private long phone;

	@Enumerated(EnumType.STRING)
	private UserStatus userStatus;
	
	private String activationCode;
	
	@Version
	private int optLockVersion;
	
	private Long QAempId;
	
	
	public String getActivationCode() {
		return activationCode;
	}
	
	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}
	
	public int getOptLockVersion() {
		return optLockVersion;
	}

	public Long getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public long getPhone() {
		return phone;
	}

	public UserStatus getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(UserStatus userStatus) {
		this.userStatus = userStatus;
	}

	public UserRoles getUserRole() {
		return userRole;
	}
	
	public void setUserRole(UserRoles userRole) {
		this.userRole = userRole;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		User user = (User) obj;
		return Objects.equal(this.id, user.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.id);
	}

	public Long getQAempId() {
		return QAempId;
	}

	public void setQAempId(Long qAempId) {
		QAempId = qAempId;
	}
	
	
}
