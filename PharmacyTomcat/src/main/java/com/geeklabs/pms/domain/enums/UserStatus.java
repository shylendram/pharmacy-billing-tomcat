package com.geeklabs.pms.domain.enums;

public enum UserStatus {
	NEW, ACTIVE, BANNED, BLOCKED, INACTIVE;
}
