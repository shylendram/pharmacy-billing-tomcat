package com.geeklabs.pms.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Rack {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String rackName;
	private Long rackColoumnNo;
	private Long rackRowNo;
	private String description;
	private Date createdDate;
	
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public String getRackName() {
		return rackName;
	}
	public void setRackName(String rackName) {
		this.rackName = rackName;
	}
	public Long getRackColoumnNo() {
		return rackColoumnNo;
	}
	public void setRackColoumnNo(Long rackColoumnNo) {
		this.rackColoumnNo = rackColoumnNo;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getRackRowNo() {
		return rackRowNo;
	}
	public void setRackRowNo(Long rackRowNo) {
		this.rackRowNo = rackRowNo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getId() {
		return id;
	}

	
}
