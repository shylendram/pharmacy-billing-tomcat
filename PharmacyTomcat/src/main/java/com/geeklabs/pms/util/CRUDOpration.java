package com.geeklabs.pms.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;


public abstract class CRUDOpration<T> implements CRUD<T> {

	protected abstract EntityManager getEntityManager();
	
	@Override
	public <S extends T> void merge(S entity) {
		getEntityManager().merge(entity);
	}
	
	@Override
	public <S extends T> void save(S entity) {
		getEntityManager().persist(entity);
		getEntityManager().flush();
	}

	@Override
	public <S extends T> void save(Iterable<S> entities) {
		getEntityManager().persist(entities);
		getEntityManager().flush();
	}

	@Override
	public T findOne(Class<T> t, Long id) {
		return getEntityManager().find(t, id);
	}


	@Override
	public void delete(Class<T> t, Long id) {
		EntityManager entityManager = getEntityManager();
		T find = entityManager.find(t, id);
		entityManager.remove(find);
	}

	@Override
	public void delete(Iterable<? extends T> entities) {
		getEntityManager().remove(entities);
	}
	
	@Override
	public List<T> findAllByCreatedDate(Class<T> t) {
		CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> createQuery = criteriaBuilder.createQuery(t);
		
		Root<T> root = createQuery.from(t);
		createQuery.select(root);
		createQuery.orderBy(criteriaBuilder.desc(root.get("createdDate")));
		
		try {
			List<T> resultList = getEntityManager().createQuery(createQuery).getResultList();
			return resultList;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return new ArrayList<>();
	}
	
	@Override
	public List<T> findAll(Class<T> t) {
		CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> createQuery = criteriaBuilder.createQuery(t);
		
		Root<T> root = createQuery.from(t);
		createQuery.select(root);
		
		try {
			List<T> resultList = getEntityManager().createQuery(createQuery).getResultList();
			return resultList;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return new ArrayList<>();
	}
	
	/*@Override
	public CriteriaQuery<T> getCriteriaQuary(Class<T> t, CriteriaBuilder criteriaBuilder, String... s){
		CriteriaQuery<T> createQuery = criteriaBuilder.createQuery(t);
		Root<T> root = createQuery.from(t);
		createQuery.select(root);
		return createQuery;
	}*/
	
}
