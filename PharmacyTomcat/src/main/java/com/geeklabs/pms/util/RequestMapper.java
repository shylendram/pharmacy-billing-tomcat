package com.geeklabs.pms.util;

public class RequestMapper {

	public final static String USER_LOGIN_SUCCESS = "/login/success";
	public final static String USER_LOGIN_ERROR = "/login/error";
	public static final String IS_ACTIVATED = "/activate";
	
	/**Admin**/
	public static final String ADMIN = "/admin";
	public static final String SETUP = "/admin/setup";
	
	/**Home**/
	public static final String HOME = "/home";
	
	/**Medicine**/
	public static final String MEDICINE = "/medicine";
	public static final String MEDICINE_LIST = "/list/get";
	public static final String ADDMEDICINE = "/add";
	public static final String UPDATE_MEDICINE = "/update/{medicineId}";
	public static final String DELETE_MEDICINE = "/delete/{medicineId}";
	public static final String GET_MEDICINES_BY_FILTER = "/filter/{medicineName}";
	
	/**Rack**/
	public static final String RACK = "/rack";
	public static final String ADD_RACK = "/rack/add";
	public static final String RACK_LIST = "/rack/list/get";
	public static final String UPDATE_RACK = "/rack/update/{rackId}";
	public static final String DELETE_RACK = "/rack/delete/{rackId}";
	
	/**Expire**/
	public static final String EXPIRE = "/expire";
	public static final String EXPIRE_LIST = "/expire/get/listExpired";
	
	/**Billing**/
	public static final String BILLING = "/billing";
	public static final String ADD_BILLING = "/add";
	public static final String GET_BILLING_LIST = "/list";
	public static final String GET_BILL_PDF = "/getBill";
	public static final String GET_PATIENT_ID = "/getPatientId";
	public static final String GET_BILL_NO = "/getBillNo";
	public static final String GET_BILL_BY_PATIENT_ID = "/get/bill/{patientId}";
}
