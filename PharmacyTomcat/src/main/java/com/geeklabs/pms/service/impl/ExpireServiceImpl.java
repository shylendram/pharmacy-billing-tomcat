package com.geeklabs.pms.service.impl;

import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.pms.domain.Medicine;
import com.geeklabs.pms.dto.MedicineDto;
import com.geeklabs.pms.repository.MedicineRepository;
import com.geeklabs.pms.service.ExpireService;
import com.geeklabs.pms.transformer.Transformer;

@Service
public class ExpireServiceImpl implements ExpireService {

	@Autowired
	private MedicineRepository medicineRepository;
	
	@Autowired
	private DozerBeanMapper dozerBeanMapper;
	
	@Override
	@Transactional
	public List<MedicineDto> getExpiryMedicine() {
		List<Medicine> medicines = medicineRepository.getExpiryMedicine();
		List<MedicineDto> medicineDtos = Transformer.convertMedicineListtoMedicineDtoList(medicines,dozerBeanMapper);
		return medicineDtos;
	}

}
