package com.geeklabs.pms.service.impl;

import java.util.Date;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.pms.domain.Rack;
import com.geeklabs.pms.dto.RackDto;
import com.geeklabs.pms.repository.RackRepository;
import com.geeklabs.pms.service.RackService;
import com.geeklabs.pms.transformer.Transformer;
import com.geeklabs.pms.util.ResponseStatus;

@Service
public class RackServiceImpl implements RackService {
	
	@Autowired
	private DozerBeanMapper dozerBeanMapper;
	
	@Autowired
	private RackRepository rackRepository;

	
	@Override
	@Transactional
	public ResponseStatus saveRack(RackDto rackDto) {
		ResponseStatus responseStatus = new ResponseStatus();
		Rack rack = dozerBeanMapper.map(rackDto, Rack.class);
		rack.setCreatedDate(new Date());
		rackRepository.save(rack);
		responseStatus.setStatus("success");
		return responseStatus;
	}

	@Override
	@Transactional
	public ResponseStatus deleteRack(Long rackId) {
		rackRepository.delete(rackId);
		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus.setStatus("success");
		return responseStatus;
	}

	@Override
	@Transactional
	public List<RackDto> getAllRacks() {
		Rack r = new Rack();
		List<Rack> racks = rackRepository.findAllByCreatedDate(r);
		List<RackDto> rackDtos = Transformer.convertRackListtoRackDtoList(racks, dozerBeanMapper);
		return rackDtos;
	}

	@Override
	@Transactional
	public ResponseStatus updateRack(RackDto rackDto) {
		ResponseStatus responseStatus = new ResponseStatus();
		
		Rack rack = rackRepository.findOne(rackDto.getId());
		dozerBeanMapper.map(rackDto, rack);
		rackRepository.merge(rack);
		responseStatus.setStatus("success");
		return responseStatus;
		
	}

	@Override
	public RackDto getRackbyId(Long rackId) {
		Rack rack = rackRepository.findOne(rackId);
		RackDto rackDto = dozerBeanMapper.map(rack, RackDto.class);
		return rackDto;
	}

}
