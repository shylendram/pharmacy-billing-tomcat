package com.geeklabs.pms.service;

import java.util.List;

import com.geeklabs.pms.dto.BillingDto;
import com.geeklabs.pms.util.ResponseStatus;

public interface BillingService {

	ResponseStatus saveBilling(BillingDto billingDto);

	List<BillingDto> getAllBillings();

	String getPatientId();

	List<BillingDto> getBilligInfoByPatientId(String patientId);

	Long getBillNo();
}
