package com.geeklabs.pms.service.impl;

import java.util.Date;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.pms.domain.Billing;
import com.geeklabs.pms.dto.BillingDto;
import com.geeklabs.pms.repository.BillingRepository;
import com.geeklabs.pms.service.BillingService;
import com.geeklabs.pms.transformer.Transformer;
import com.geeklabs.pms.util.ResponseStatus;

@Service
public class BillingServiceImpl implements BillingService {

	@Autowired
	private DozerBeanMapper dozerBeanMapper;

	@Autowired
	private BillingRepository billingRepository;

	@Override
	@Transactional
	public ResponseStatus saveBilling(BillingDto billingDto) {
		ResponseStatus responseStatus = new ResponseStatus();
		// Billing billing = dozerBeanMapper.map(billingDto, Billing.class);
		Billing billing = extracted(billingDto);
		billingRepository.save(billing);
		responseStatus.setStatus("success");
		return responseStatus;
	}

	private Billing extracted(BillingDto billingDto) {
		Billing billing = new Billing();
		billing.setBatchNo(billingDto.getBatchNo());
		billing.setDate(new Date());
		billing.setDrName(billingDto.getDrName());
		billing.setManufactureCompany(billingDto.getManufactureCompany());
		billing.setExpireDate(billingDto.getExpireDate());
		billing.setManufactureDate(billingDto.getManufactureDate());
		billing.setMedicineName(billingDto.getMedicineName());
		billing.setPatientName(billingDto.getPatientName());
		billing.setPrice(billingDto.getPrice());
		billing.setTotalPrice(billingDto.getTotalPrice());
		billing.setQuantity(billingDto.getQuantity());
		billing.setScheduleType(billingDto.getScheduleType());
		billing.setCode(billingRepository.getMaxCodeNumber() + 1);
		billing.setPatientId(billingDto.getPatientId());
//		billing.setBillNo(billingDto.getBillNo() + 1l);
		return billing;
	}

	@Override
	@Transactional
	public List<BillingDto> getAllBillings() {
		Billing b = new Billing();
		List<Billing> billings = billingRepository.findAllByCreatedDate(b);
		List<BillingDto> billingDtoList = Transformer.convertBillingListToBillingDtoList(billings, dozerBeanMapper);
		return billingDtoList;
	}

	@Override
	public String getPatientId() {
		String prefix = "svnp0";
		long maxCode = billingRepository.getMaxCodeNumber() + 1;
		String patientId = prefix + maxCode;
		return patientId;
	}
	
	@Override
	@Transactional
	public List<BillingDto> getBilligInfoByPatientId(String patientId) {
		List<Billing> billingInfoList = billingRepository.getBillingInfoByPatientId(patientId);
		List<BillingDto> billingDtoList = Transformer.convertBillingListToBillingDtoList(billingInfoList, dozerBeanMapper);
		return billingDtoList;
	}

	@Override
	public Long getBillNo() {
		long maxCodeNumber = billingRepository.getMaxCodeNumber() + 1;
		return maxCodeNumber;
	}
}
