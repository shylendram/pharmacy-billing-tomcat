package com.geeklabs.pms.service;

import java.util.List;

import com.geeklabs.pms.dto.MedicineDto;

public interface ExpireService {

	List<MedicineDto> getExpiryMedicine();

}
