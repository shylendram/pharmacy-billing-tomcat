package com.geeklabs.pms.service;

import com.geeklabs.pms.domain.UserRole;
import com.geeklabs.pms.domain.enums.UserRoles;

public interface UserRoleService {

	UserRole getUserRoleByRoleName(UserRoles roles);

	
}
