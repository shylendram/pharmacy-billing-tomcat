package com.geeklabs.pms.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.geeklabs.pms.domain.User;
import com.geeklabs.pms.repository.UserRepository;
import com.geeklabs.pms.util.ResponseStatus;

public interface UserService extends UserDetailsService {

	void setupAppUserRoles();
	void setupAppUser();
	
	void setUserRepository(UserRepository userRepository);
	void setPasswordEncoder(BCryptPasswordEncoder passwordEncoder);
	
	ResponseStatus deleteUser(Long id);
	User getUserByEamailOrUserName(String userName, String email);
	User getUserByName(String userName);
}
