package com.geeklabs.pms.service;

import java.util.List;

import com.geeklabs.pms.dto.RackDto;
import com.geeklabs.pms.util.ResponseStatus;

public interface RackService {
	
	ResponseStatus saveRack(RackDto rackDto);
	ResponseStatus deleteRack(Long rackId);
	
	List<RackDto> getAllRacks();
	ResponseStatus updateRack(RackDto rackDto);
	RackDto getRackbyId(Long rackId);

}
