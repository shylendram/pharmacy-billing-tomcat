package com.geeklabs.pms.service.impl;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.pms.domain.Medicine;
import com.geeklabs.pms.dto.MedicineDto;
import com.geeklabs.pms.dto.RackDto;
import com.geeklabs.pms.repository.MedicineRepository;
import com.geeklabs.pms.service.MedicineService;
import com.geeklabs.pms.service.RackService;
import com.geeklabs.pms.transformer.Transformer;
import com.geeklabs.pms.util.ResponseStatus;

@Service
public class MedicineServiceImpl implements MedicineService {
	
	@Autowired
	private DozerBeanMapper dozerBeanMapper;
	
	@Autowired
	private MedicineRepository medicineRepository;
	
	@Autowired
	private RackService rackService;

	@Override
	@Transactional
	public ResponseStatus saveMedicine(MedicineDto medicineDto) {
		ResponseStatus responseStatus = new ResponseStatus();
		Medicine medicine = dozerBeanMapper.map(medicineDto, Medicine.class);
		
		//set Medicine Expire date from String to Date Format
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date expireDate = null;
		try {
			expireDate = dateFormat.parse(medicineDto.getExpireDate());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		medicine.setExpiryDate(expireDate);
		medicine.setCreatedDate(new Date());
		medicineRepository.save(medicine);
		responseStatus.setStatus("success");
		return responseStatus;
	}

	@Override
	@Transactional
	public ResponseStatus deleteMedicine(Long medicineId) {
		medicineRepository.delete(medicineId);
		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus.setStatus("success");
		return responseStatus;
	}

	@Override
	@Transactional
	public List<MedicineDto> getAllMedicine() {
		Medicine m = new Medicine();
		List<Medicine> medicines = medicineRepository.findAllByCreatedDate(m);
		List<MedicineDto> medicineDtos = Transformer.convertMedicineListtoMedicineDtoList(medicines,dozerBeanMapper);
		return medicineDtos;
	}

	@Override
	@Transactional
	public ResponseStatus updateMedicine(MedicineDto medicineDto) {
		ResponseStatus responseStatus = new ResponseStatus();
		
		Medicine medicine = medicineRepository.findOne(medicineDto.getId());
		dozerBeanMapper.map(medicineDto, medicine);
		medicineRepository.merge(medicine);
		responseStatus.setStatus("success");
		return responseStatus;
	}

	@Override
	public MedicineDto getMedicinebyId(Long medicineId) {
		Medicine medicine = medicineRepository.findOne(medicineId);
		MedicineDto medicineDto = new MedicineDto();
		
		// converting Date to String format
		Date expiryDate = medicine.getExpiryDate();
		Format formatter = new SimpleDateFormat("yyyy-MM-dd");
		String expireDate = formatter.format(expiryDate);
		
		extracted(medicine, medicineDto, expireDate);
		
		List<RackDto> rackList = rackService.getAllRacks();
		medicineDto.setRacks(rackList);
		
		return medicineDto;
	}

	private void extracted(Medicine medicine, MedicineDto medicineDto, String expireDate) {
		medicineDto.setExpireDate(expireDate);
		medicineDto.setBatchNo(medicine.getBatchNo());
		medicineDto.setManufactureCompany(medicine.getManufactureCompany());
		medicineDto.setManufactureDate(medicine.getManufactureDate());
		medicineDto.setMedicineName(medicine.getMedicineName());
		medicineDto.setPrice(medicine.getPrice());
		medicineDto.setQuantity(medicine.getQuantity());
		medicineDto.setRackName(medicine.getRackName());
		medicineDto.setScheduleType(medicine.getScheduleType());
		medicineDto.setId(medicine.getId());
	}

	@Override
	public List<MedicineDto> getAllMedicinesByMedicineName(String name) {
		List<Medicine> medicines = medicineRepository.getAllMedicinesByMedicineName(name);
		List<MedicineDto> medicineDtos = Transformer.convertMedicineListtoMedicineDtoList(medicines, dozerBeanMapper);
		return medicineDtos;
	}

}
