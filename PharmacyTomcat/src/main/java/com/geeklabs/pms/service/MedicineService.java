package com.geeklabs.pms.service;


import java.util.List;

import com.geeklabs.pms.dto.MedicineDto;
import com.geeklabs.pms.util.ResponseStatus;

public interface MedicineService {
	
	ResponseStatus saveMedicine(MedicineDto medicineDto);
	ResponseStatus deleteMedicine(Long medicineId);
	
	List<MedicineDto> getAllMedicine();
	ResponseStatus updateMedicine(MedicineDto medicineDto);
	MedicineDto getMedicinebyId(Long medicineId);
	List<MedicineDto> getAllMedicinesByMedicineName(String name);

}
